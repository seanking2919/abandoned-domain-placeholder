# Abandoned Domain Placeholder

A simple placeholder static page I use for any domains I'm not using anymore and want to give away and/or sell.

## Running locally

To get this running, first clone the repo:

```
git clone https://gitlab.com/seanking2919/abandoned-domain-placeholder.git
cd abandoned-domain-placeholder
```

Then run with a local webserver such as [`http-server`](https://www.npmjs.com/package/http-server) like seen below:

```
http-server -c-1 public
```